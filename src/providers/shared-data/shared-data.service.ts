
import { Injectable, ApplicationRef, NgZone } from '@angular/core';
import { ConfigService } from '../config/config.service';

import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { LoadingService } from '../loading/loading.service';
import { Platform, ToastController, AlertController, NavController } from '@ionic/angular';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { Device } from '@ionic-native/device/ngx';
import { AppEventsService } from 'src/providers/app-events/app-events.service';

@Injectable()
export class SharedDataService {

  public banners = [];
  public customerData: { [k: string]: any } = {};

  public tempdata: { [k: string]: any } = {};
  public dir = "ltr";
  public currentOpenedModel: any = null;
  statusOfBoy = false;
  public myCurrentLocation: { [k: string]: any } = {};
  public translationListArray = [];

  public missingValues = [];
  contentPages: any;
  lab = false;
  orderDetailPageData: any;
  mapPageData: { lat: any; long: any; };
  constructor(
    public config: ConfigService,
    public httpClient: HttpClient,
    public storage: Storage,
    public loading: LoadingService,
    public appEventsService: AppEventsService,
    public platform: Platform,
    public device: Device,
    public fcm: FCM,
    public alertCtrl: AlertController,
    public appVersion: AppVersion,
    public oneSignal: OneSignal,
    private toastCtrl: ToastController,
    public splashScreen: SplashScreen,
    private navCtrl: NavController,
    private http: HttpClient
  ) {

    this.http.get('assets/i18n/' + 'en' + ".json").subscribe((data: any) => {
      this.translationListArray = data;
    });
    setTimeout(() => {
      this.lab = true;
    }, 10000);


    this.config.getHttp("pages?language_id=" + localStorage.langId).then((data: any) => {
      if (data.success == "1")
        this.contentPages = data.pages_data;
    });

    let settingsLoaded = this.appEventsService.subscribe("settingsLoaded");
    settingsLoaded.subscriptions.add(settingsLoaded.event.subscribe(data => {
      this.onStart();
    }));
    //---------------- end -----------------

  }
  public splashScreenHide = false;
  hideSplashScreen() {
    if (this.platform.is('cordova')) {
      if (!this.splashScreenHide) { this.splashScreen.hide(); this.splashScreenHide = true; }
    }
  }
  onStart() {

  }

  login(data) {
    this.customerData = data;
    this.storage.set('customerData', this.customerData);
    console.log(this.customerData);
    this.navCtrl.navigateRoot("tabs");
  }

  logOut() {
    if (this.statusOfBoy == true)
      this.statusOfBoy = false
    this.changeStatus();

    this.navCtrl.navigateRoot("login");
    this.loading.autoHide(500);
    this.customerData = {};
    this.storage.set('customerData', this.customerData);
    // this.fb.logout();
  }

  //============================================================================================
  //registering device for push notification function
  registerDevice(registrationId) {
    //this.storage.get('registrationId').then((registrationId) => {
    console.log(registrationId);
    if(registrationId === null || registrationId === "")return
    let data: { [k: string]: any } = {};
    if (this.customerData.customers_id == null)
      data.customers_id = null;
    else
      data.customers_id = this.customerData.customers_id;
    //	alert("device ready fired");
    let deviceInfo = this.device;
    data.device_model = deviceInfo.model;
    data.device_type = deviceInfo.platform;
    data.device_id = registrationId;
    data.device_os = deviceInfo.version;
    data.manufacturer = deviceInfo.manufacturer;
    data.ram = '2gb';
    data.processor = 'mediatek';
    data.location = 'empty';

    // alert(JSON.stringify(data));
    this.config.postHttp("registerdevices", data).then(data => {
      //  alert(registrationId + " " + JSON.stringify(data));
    });
    //  });

  }

  showAd() {
    //this.loading.autoHide(2000);
    this.appEventsService.publish('showAd', "");
  }

  toast(msg, time = 3500) {
    this.translateString(msg).then(async (res: string) => {
      const toast = await this.toastCtrl.create({
        message: res,
        duration: time,
        position: 'bottom'
      });
      toast.present();
    });
  }
  toastMiddle(msg) {

    this.translateString(msg).then(async (res: string) => {
      let toast = await this.toastCtrl.create({
        message: res,
        duration: 3500,
        position: 'middle'
      });

      toast.present();
    });
  }

  toastWithCloseButton(msg) {

    this.translateString(msg).then(async (res: string) => {
      let toast = await this.toastCtrl.create({
        message: res,
        keyboardClose: true,
        position: 'middle',
        //text: "X"
      });
      toast.present();
    });
  }


  // translation services
  translateString(value) {
    return new Promise(resolve => {
      let v = this.translationListArray[value];
      console.log(v);
      if (v == undefined) {
        this.missingValues[value] = value;
        v = value;
      }
      resolve(v);
    });
  }
  translateArray(value) {
    return new Promise(resolve => {
      let tempArray = [];
      value.forEach(element => {
        if (this.translationListArray[element] != undefined) {
          tempArray[element] = this.translationListArray[element];

        }
        else {
          tempArray[element] = element;
          this.missingValues[value] = value;
        }
      });
      resolve(tempArray);
    });
  }
  //=================================================

  showAlert(text) {
    this.translateArray([text, "ok", "Alert"]).then(async (res) => {
      console.log(res);
      const alert = await this.alertCtrl.create({
        header: res["Alert"],
        message: res[text],
        buttons: [res["ok"]]
      });
      await alert.present();
    });
  }

  showAlertWithTitle(text, title) {
    this.translateArray([text, "ok", title]).then(async (res) => {
      let alert = await this.alertCtrl.create({
        header: res[title],
        message: res[text],
        buttons: [res["ok"]]
      });
      await alert.present();

    });
  }

  getNameFirstLetter() {
    return this.customerData.first_name.charAt(0);
  }


  userIsLoggedIn() {
    return new Promise(resolve => {
      this.storage.get('customerData').then((val) => {
        if (val != null || val != undefined) this.customerData = val;
        if (this.customerData.id != undefined)
          resolve(true);
        else
          resolve(false);
      });
    });
  }


  changeStatus() {
    let status = 11
    if (this.statusOfBoy == true)
      status = 8
    this.loading.show();
    this.config.getHttp('changestatus?password=' + this.customerData.password + '&availability_status=' + status).then((data: any) => {
      this.loading.hide();
      if (data.success == 1) {
        if (this.statusOfBoy) {
          this.navCtrl.navigateRoot("tabs/tabs/home");
          this.appEventsService.publish("startBackgroundLocation", "");
        }
        else {
          this.appEventsService.publish("stopBackgroundLocation", "");
        }
        //this.toast(data.message);
      }
      if (data.success == 0) {
        this.toast(data.message);
      }
    });
  }
}
