import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { LoginPage } from 'src/app/modals/login/login.page';
import { SharedDataService } from '../shared-data/shared-data.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    public modalCtrl: ModalController,
    public shared: SharedDataService,
    private navCtrl: NavController,
  ) {

  }
  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (this.shared.customerData.id == null) {
      this.openLoginPage(route.data.hideGuestLogin);
    }
    else
      return true;
  }

  openLoginPage(value) {
    this.navCtrl.navigateRoot("/login")
  }
}
