
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Md5 } from 'ts-md5/dist/md5';
import { HTTP } from '@ionic-native/http/ngx';
import { AppEventsService } from 'src/providers/app-events/app-events.service';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { LoadingService } from '../loading/loading.service';
import { GetIpAddressService } from '../get-ip-Address/get-ip-address.service';
import { GetDeviceIdService } from '../get-device-id/get-device-id.service';

if (localStorage.langId == undefined) {

  localStorage.langId = '1';//default language id
  localStorage.languageCode = "es"; //default language code
  localStorage.direction = "ltr"; //default language direction of app
  localStorage.currency = "€";  //default currecny html code to show in app.
  // Please visit this link to get your html code  https://html-css-js.com/html/character-codes/currency/
  localStorage.currencyCode = "EUR";  //default currency code
  localStorage.currencyPos = "right";  //default currency position
  localStorage.decimals = 2;  //default currecny decimal
}

@Injectable()

export class ConfigService {


  public yourSiteUrl: string = 'https://appu.es';
  public consumerKey: string = "54f1998a1594406259010e9e1e";
  public consumerSecret: string = "1efba47d159440625961f43559";


  public showIntroPage = 1; //  0 to hide and 1 to show intro page
  public appInProduction = true;//  0 to hide and 1 to show intro page
  public defaultIcons = true; //  0 to hide and 1 to show intro page

  // Initialize Firebase
  public firebaseConfig: { [k: string]: any } = {};


  public appTheme = 'default';
  public darkMode = false;


  public url: string = this.yourSiteUrl + '/deliveryboy/';
  public imgUrl: string = this.yourSiteUrl + "/";
  public langId: string = localStorage.langId;
  public currecnyCode: string = localStorage.currencyCode;
  public currency = localStorage.currency;
  public currencyPos = localStorage.currencyPos;

  appSettings: any;
  enableAddressMap: boolean = false;
  googleMapId: 'AIzaSyCFQFHqhnvjBZkDl6McMSet85AwWndiDDY';
  oneSignalDeviceId: any;
  openDeviceMap = true;


  constructor(
    public storage: Storage,
    public platform: Platform,
    public oneSignal: OneSignal,
    public md5: Md5,
    public http: HttpClient,
    public loading: LoadingService,
    public appEventsService: AppEventsService,
    private httpNative: HTTP,
    public getIpAddressService: GetIpAddressService,
    public getDeviceIdService: GetDeviceIdService
  ) {
    this.consumerKey = Md5.hashStr(this.consumerKey).toString();
    this.consumerSecret = Md5.hashStr(this.consumerSecret).toString();
  }
  getHeadersForHttp() {
    let d = new Date();
    let nonce = d.getTime().toString();
    let headers = {
      'consumer-key': this.consumerKey,
      'consumer-secret': this.consumerSecret,
      'consumer-nonce': nonce,
      'consumer-device-id': this.getDeviceIdService.getDeviceId(),
      'consumer-ip': this.getIpAddressService.getIpAddress(),
      'Content-Type': 'application/json',
    };
    return headers;
  }



  getHttp(req) {
    let customHeaders = this.getHeadersForHttp()
    const httpOptions = {
      headers: new HttpHeaders(customHeaders)
    };

    return new Promise(resolve => {
      if (this.platform.is('cordova')) {
        this.httpNative.get(this.url + req, {}, customHeaders)
          .then(data => {
            let d = JSON.parse(data.data);
            //this.storeHttpData(request, d);
            resolve(d);
            //console.log(data.status);
            //console.log(data.data); // data received by server
            //console.log(data.headers);
          })
          .catch(error => {
            // console.log("Error : " + req);
            // console.log(error);
            // console.log(error.error); // error message as string
            // console.log(error.headers);
          });
      }
      else {

        this.http.get(this.url + req, httpOptions).subscribe((data: any) => {
          resolve(data);
        }, (err) => {
          console.log("Error : " + req);
          console.log(err);
        });
      }
    });
  }
  postHttp(req, data) {
    let customHeaders = this.getHeadersForHttp()
    const httpOptions = {
      headers: new HttpHeaders(customHeaders)
    };

    return new Promise(resolve => {
      if (this.platform.is('cordova')) {
        this.httpNative.setDataSerializer("json");
        this.httpNative.post(this.url + req, data, customHeaders)
          .then(data => {
            let d = JSON.parse(data.data);
            //console.log(this.url + req, d);
            //this.storeHttpData(request, d);
            resolve(d);
            //console.log(data.status);
            //console.log(data.data); // data received by server
            //console.log(data.headers);
          })
          .catch(error => {
            // console.log("Error : " + req);
            // console.log(error);
            // console.log(error.error); // error message as string
            // console.log(error.headers);
          });
      }
      else {
        this.http.post(this.url + req, data, httpOptions).subscribe((data: any) => {
          resolve(data);
        }, (err) => {
          console.log("Error : " + req);
          console.log(err);
        });
      }
    });
  }

  public siteSetting() {
    return new Promise(resolve => {
      this.storage.get('appSettings').then((val) => {
        if (val == null) {
          this.getSettingsFromServer().then((data: any) => {
            if (data.success == "1") {
              this.appSettings = data.data;
              this.storage.set("appSettings", this.appSettings);
              this.defaultSettings();
              this.appEventsService.publish('settingsLoaded', "");
            }
            resolve();
          });
        }
        else {
          this.appSettings = val;
          this.defaultSettings();
          this.appEventsService.publish('settingsLoaded', "");
          resolve();
        }
      });
    });
  }
  defaultSettings() {
    localStorage.currency = this.appSettings.currency_symbol
    this.googleMapId = this.appSettings.google_map_api
    this.enableAddressMap = (this.appSettings.is_enable_location == "1") ? true : false;

    if (this.platform.is('cordova')) {
      this.platform.ready().then(() => {
        this.initializeOneSignal(this.appSettings.onesignal_app_id, this.appSettings.onesignal_sender_id);
      });
    }


    this.firebaseConfig = {
      apiKey: this.appSettings.firebase_apikey,
      authDomain: this.appSettings.auth_domain,
      databaseURL: this.appSettings.database_URL,
      projectId: this.appSettings.projectId,
      storageBucket: this.appSettings.storage_bucket,
      messagingSenderId: this.appSettings.messaging_senderid,
    };

    // "auth_domain": "Auth Domain",
    // "currency_symbol": "$",
    // "database_URL": "Database URL",
    // "default_latitude": "",
    // "default_longitude": "1",
    // "firebase_apikey": "",
    // "google_map_api": "",
    // "is_enable_location": "1",
    // "messaging_senderid": "Messaging SenderID",
    // "onesignal_app_id": "",
    // "onesignal_sender_id": "",
    // "projectId": "",
    // "storage_bucket": ""

  }

  initializeOneSignal(appId, senderId) {

    this.oneSignal.startInit(appId, senderId);
    this.oneSignal.endInit();
    this.oneSignal.getIds().then((data) => {
      this.oneSignalDeviceId = data.userId
      console.log(this.oneSignalDeviceId);
    })

  }

  checkingNewSettingsFromServer() {
    this.loading.show();
    this.getSettingsFromServer().then((data: any) => {
      this.loading.hide();
      if (data.success == "1") {
        var settings = data.data;
        this.reloadingWithNewSettings(settings);
      }
    });
  }
  reloadingWithNewSettings(data) {
    if (JSON.stringify(this.appSettings) !== JSON.stringify(data)) {
      //if (data.wp_multi_currency == "0") this.restoreDefaultCurrency();
      this.storage.set("appSettings", data).then(function () {
      });
    }
  }


  getSettingsFromServer() {
    return this.getHttp('setting');
  }

}
