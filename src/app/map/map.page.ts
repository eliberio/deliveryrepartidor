import { Component, OnInit } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { GoogleMaps, GoogleMapsEvent, LatLng, MarkerOptions, Marker, Environment } from "@ionic-native/google-maps";
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { UserAddressService } from 'src/providers/user-address/user-address.service';
import { LoadingService } from 'src/providers/loading/loading.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ConfigService } from 'src/providers/config/config.service';
import { AppEventsService } from 'src/providers/app-events/app-events.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  data: any = {};
  myLocation: any = {};

  constructor(
    public modalCtrl: ModalController,
    public platform: Platform,
    public shared: SharedDataService,
    public config: ConfigService,
    public loading: LoadingService,
    public appEventsService: AppEventsService,
    public userAddress: UserAddressService,
    private geolocation: Geolocation
  ) {


  }
  // ngAfterViewInit() {
  //   this.platform.ready().then(() => this.loadMap());
  // }

  loadMap() {
    // This code is necessary for browser
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': this.config.googleMapId,
      //'API_KEY_FOR_BROWSER_DEBUG': this.config.googleMapId
    });
    /* The create() function will take the ID of your map element */
    const map = GoogleMaps.create('map');

    map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {
      const coordinates: LatLng = new LatLng(this.myLocation.lat, this.myLocation.long);

      map.setCameraTarget(coordinates);
      map.setCameraZoom(10);
    });
    let myLocationMarker;
    this.shared.translateString("My Location").then((data: any) => {
      map.addMarker({
        position: { lat: this.myLocation.lat, lng: this.myLocation.long },
        title: data,
        icon: 'blue'
      }).then((m: any) => {

        let locationUpdated = this.appEventsService.subscribe("locationUpdated");
        locationUpdated.subscriptions.add(locationUpdated.event.subscribe(data => {
          m.setPosition(new LatLng(data.latitude, data.longitude));
          console.log("locationUpdated", data);
        }));

      }, (error) => {
        console.log(error);
      });
    })
    this.shared.translateString("Destination").then((data: any) => {
      map.addMarker({
        position: { lat: this.data.lat, lng: this.data.long },
        title: data,
      })
    })
  }
  ngOnInit() {

  }
  ionViewDidEnter() {
    this.data = this.shared.mapPageData;
    this.loading.show();
    let locationEnable = false
    this.userAddress.getCordinates().then(res => {
      locationEnable = true;
      this.loading.hide();
      this.myLocation = res;
      this.loadMap()
    });

    setTimeout(() => {
      if (locationEnable == false) {
        this.shared.showAlert("Please Turn On Device Location");
      }
    }, 5000);
  }

}
