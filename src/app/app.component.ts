import { Component, NgZone, ViewChildren, QueryList } from '@angular/core';
import { Platform, NavController, ModalController, MenuController, IonRouterOutlet } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ConfigService } from 'src/providers/config/config.service';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { Network } from '@ionic-native/network/ngx';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AppEventsService } from 'src/providers/app-events/app-events.service';
import * as firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/database';
import { UserAddressService } from 'src/providers/user-address/user-address.service';
import { BackgroundGeolocation, BackgroundGeolocationEvents, BackgroundGeolocationResponse, BackgroundGeolocationConfig } from '@ionic-native/background-geolocation/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BackButtonExitAppService } from 'src/providers/back-button-exit/back-button-exit-app.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  rootPage: any;
  constructor(
    public shared: SharedDataService,
    public config: ConfigService,
    public router: Router,
    private navCtrl: NavController,
    public modalCtrl: ModalController,
    public statusBar: StatusBar,
    public storage: Storage,
    public network: Network,
    public appEventsService: AppEventsService,
    public plt: Platform,
    private backgroundGeolocation: BackgroundGeolocation,
    public loc: UserAddressService,
    private geolocation: Geolocation,
    public backButtonExit: BackButtonExitAppService,
  ) {
    this.plt.ready().then(() => {
      this.statusBar.styleDefault();
    });
    let connectedToInternet = true;
    network.onDisconnect().subscribe(() => {
      connectedToInternet = false;
      this.shared.showAlertWithTitle("Please Connect to the Internet", "Disconnected");
    });

    network.onConnect().subscribe(() => {
      if (!connectedToInternet) {
        window.location.reload();
        this.shared.showAlertWithTitle("Network connected Reloading Data" + '...', "Connected");
      }
    });

    document.documentElement.dir = localStorage.direction;
    shared.dir = localStorage.direction;

    this.initializeApp();

  }
  checkUserLocationIsEnabled() {
    let count = 0
    this.loc.getCordinates().then((d) => {
      count = 1
      console.log(d)
    });

    setTimeout(() => {
      if (count == 0) {
        this.shared.showAlert("Please Turn On Device Location");
        //this.checkUserLocationIsEnabled();
      }
      else {
        console.log("location Is enabled");
      }
    }, 10000);
  }
  ionAlertWillDismiss() {
    console.log("alert closed");
  }
  click() {
    console.log(this.shared.missingValues);
  }
  updateLocationOnFirebaseDatabase() {

    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 20,
      distanceFilter: 30,
      interval: 10000,
      startOnBoot: true,
      startForeground: true,
      fastestInterval: 5000,
      activitiesInterval: 10000,
      debug: !this.config.appInProduction, //  enable this hear sounds for background-geolocation life-cycle.
      stopOnTerminate: false, // enable this to clear background location settings when the app terminates
    };

    this.backgroundGeolocation.configure(config)
      .then(() => {
        this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {
          console.log(location);
          this.shared.myCurrentLocation = { latitude: location.latitude, longitude: location.longitude, source: "app Background" }
          firebase.database().ref('location/' + this.shared.customerData.deliveryboy_id).set({ latitude: location.latitude, longitude: location.longitude, source: "app Background" });
          this.appEventsService.publish("locationUpdated", this.shared.myCurrentLocation)
          // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
          // and the background-task may be completed.  You must do this regardless if your operations are successful or not.
          // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.

          let stopBackgroundLocation = this.appEventsService.subscribe("stopBackgroundLocation");
          stopBackgroundLocation.subscriptions.add(stopBackgroundLocation.event.subscribe(data => {
            // stop recording location
            this.backgroundGeolocation.finish(); // FOR IOS ONLY
            console.log("finishBackgroundLocation");
          }));
        });

      });

    let startBackgroundLocation = this.appEventsService.subscribe("startBackgroundLocation");
    startBackgroundLocation.subscriptions.add(startBackgroundLocation.event.subscribe(data => {
      // start recording location
      this.backgroundGeolocation.start();
      console.log("startBackgroundLocation");
    }));

    let stopBackgroundLocation = this.appEventsService.subscribe("stopBackgroundLocation");
    stopBackgroundLocation.subscriptions.add(stopBackgroundLocation.event.subscribe(data => {
      // stop recording location
      this.backgroundGeolocation.stop();
      console.log("stopBackgroundLocation");
    }));

    // let watch = this.geolocation.watchPosition();
    // watch.subscribe((data) => {
    //   this.appEventsService.publish("locationUpdated", { latitude: data.coords.latitude, longitude: data.coords.longitude })
    //   console.log("gelocation", data.coords);
    //   firebase.database().ref('location/' + this.shared.customerData.deliveryboy_id).set({ latitude: data.coords.latitude, longitude: data.coords.longitude, source: "app Visible" });
    // });
  }

  initializeApp() {

    this.plt.ready().then(() => {
      this.config.siteSetting().then((value) => {
        firebase.initializeApp(this.config.firebaseConfig);
        this.loadHomePage()
        //subscribe for push notifiation
        this.storage.get('pushNotification').then((val) => {
          if (val == undefined) {
            this.storage.set('pushNotification', "loaded");
          }
        });
      });
      this.statusBar.styleLightContent();

    });
  }

  loadHomePage() {

    this.shared.userIsLoggedIn().then((data: any) => {
      console.log("check user");
      if (data) {
        this.navCtrl.navigateRoot("tabs");
      }
      else
        this.navCtrl.navigateRoot("login");
      this.updateLocationOnFirebaseDatabase();
      this.checkUserLocationIsEnabled();
    })

    this.storage.get('firstTime').then((val) => {
      if (val == undefined) {
        this.storage.set('firstTime', "firstTime");
      }
      else {
        this.config.checkingNewSettingsFromServer();
      }
    });
  }
  logOut() {
    this.shared.logOut();
  }

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  ngAfterViewInit() {
    this.backButtonExit.routerOutlets = this.routerOutlets;
    this.backButtonExit.backButtonEvent();
  }
}
