import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/providers/loading/loading.service';
import { ModalController, NavParams } from '@ionic/angular';
import { ConfigService } from 'src/providers/config/config.service';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  data: any;
  header: any;
  constructor(
    public loading: LoadingService,
    public modalCtrl: ModalController,
    public config: ConfigService,
    public shared: SharedDataService,
    public navParams: NavParams,
  ) {
    this.data = navParams.get('data');
    this.header = navParams.get('header');
  }
  //close modal
  dismiss() {
    this.modalCtrl.dismiss();
  }
  ngOnInit() {
  }

}
