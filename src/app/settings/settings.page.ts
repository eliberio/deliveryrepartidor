import { Component, OnInit } from '@angular/core';
import { ModalController, Platform, NavController, AlertController } from '@ionic/angular';
import { ConfigService } from 'src/providers/config/config.service';
import { LoadingService } from 'src/providers/loading/loading.service';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../modals/login/login.page';
import { AppEventsService } from 'src/providers/app-events/app-events.service';
import { ModalPage } from '../modal/modal.page';
import { MyAccountPage } from '../my-account/my-account.page';
import { MapPage } from '../map/map.page';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  setting: { [k: string]: any } = {};
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public config: ConfigService,
    private storage: Storage,
    public loading: LoadingService,
    public appEventsService: AppEventsService,
    public shared: SharedDataService,
    public plt: Platform,
    private appVersion: AppVersion,
    private oneSignal: OneSignal,
    public alertController: AlertController,
  ) {
  }
  click() {
    console.log(this.shared.missingValues);
  }
  async logoutAfterConfirm() {
    this.shared.translateArray(["Confirm", "Log Out", "Cancel", "Are you sure?"]).then(async (res: any) => {
      const alert = await this.alertController.create({
        header: res["Confirm"],
        message: res["Are you sure?"],
        buttons: [
          {
            text: res["Cancel"],
            role: 'cancel',
            handler: (blah) => {
            }
          }, {
            text: res["Log Out"],
            handler: () => {
              this.shared.logOut();
            }
          }
        ]
      });

      await alert.present();
    });
  }

  ionViewDidLoad() {
    this.storage.get('setting').then((val) => {
      if (val != null || val != undefined) {
        this.setting = val;

      }
      else {
        this.setting.localNotification = true;
        this.setting.notification = true;
        this.setting.cartButton = true;
        this.setting.footer = true;
      }
    });
  }


  getNameFirstLetter() {
    return this.shared.getNameFirstLetter();
  }

  showAd() {
    this.loading.autoHide(2000);
    this.appEventsService.publish('showAd', "");
  }

  async openPage(value) {
    let modal = await this.modalCtrl.create({
      component: ModalPage,
      componentProps: { data: value.description, header: value.name }
    });
    return await modal.present();
  }

  async openAccountPage() {

    let modal = await this.modalCtrl.create({
      component: MyAccountPage
    });
    return await modal.present();

  }

  ionViewWillEnter() {
    this.getBoyInfo(null);
  }

  getBoyInfo(value: any) {
    this.loading.show();
    this.config.getHttp('deliveryboyinfo?password=' + this.shared.customerData.password + '&language_id=1').then((data: any) => {
      this.loading.hide();
      if (data.success == 1) {
        this.shared.login(data.data[0])
        if (data.data[0].availability_status == 8)
          this.shared.statusOfBoy = true
        else
          this.shared.statusOfBoy = false
      }
      if (data.success == 0) {
        this.shared.toast(data.message)
      }
      this.stopRefresh(value);
    });
  }
  stopRefresh(value) {
    if (value != null)
      value.target.complete();
  }
  ngOnInit() {
  }

}
