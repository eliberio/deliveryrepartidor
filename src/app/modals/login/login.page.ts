import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/providers/config/config.service';
import { ModalController, NavController, AlertController } from '@ionic/angular';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { LoadingService } from 'src/providers/loading/loading.service';
import { Device } from '@ionic-native/device/ngx';

import * as firebase from 'firebase/app';
import 'firebase/auth';

declare var window;
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  formData = { password: '' };
  errorMessage = '';
  hideGuestLogin: true;
  constructor(

    public config: ConfigService,
    public modalCtrl: ModalController,
    public loading: LoadingService,
    public shared: SharedDataService,
    public navCtrl: NavController,
    public device: Device,
    public alertController: AlertController
  ) {
  }

  ngAfterViewInit() {
    this.createRecaptcha();
  }


  //=================================
  createRecaptcha() {
    let _this = this;
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('log-in-button', {
      'size': 'normal',
      'callback': function (response) {
        console.log(response);
      }
    });
  }

  public userData: any = {}
  login() {
    this.loading.show();
    this.errorMessage = '';

    let data: { [k: string]: any } = {};

    let deviceInfo = this.device;
    let dataToPost = "?password=" + this.formData.password
    dataToPost += "&device_model=" + deviceInfo.model

    dataToPost += "&device_type=" + deviceInfo.platform
    dataToPost += "&device_id=" + this.config.oneSignalDeviceId;
    dataToPost += "&device_os=" + deviceInfo.version
    dataToPost += "&manufacturer=" + deviceInfo.manufacturer
    dataToPost += "&ram=" + '2gb'
    dataToPost += "&processor=" + 'mediatek'
    dataToPost += "&location=" + 'empty'

    this.config.getHttp('login' + dataToPost).then((data: any) => {
      this.loading.hide();
      if (data.success == 1) {
        this.userData = data.data[0];
        /**TODO
         * Se descomenta para producción
        **/
        //this.verifyPhoneNumber();
        this.loginAfterCodeVerify();
        this.shared.toast("Welcome");
      }
      if (data.success == 0) {
        this.errorMessage = data.message;
      }
    });
  }

  //================================= function to verify send code
  verifyPhoneNumber() {
    this.loading.show();
    let _this = this;
    this.errorMessage = ""
    firebase.auth().signInWithPhoneNumber(this.userData.phone, window.recaptchaVerifier).then(function (confirmationResult) {
      _this.loading.hide();
      _this.enterThePhoneCodeReceived().then((data: any) => {

        if (data != null)
          confirmationResult.confirm(data).then((data: any) => {
            _this.loginAfterCodeVerify();
            _this.shared.toast("Welcome");
          }).catch(
            (error) => {
              _this.shared.showAlert(data + " " + "Código inválido intente nuevamente.");
              console.log(error)
              //_this.errorMessage = error.message
            });
      })
    }).catch(
      (error) => {
        console.log(error);
        this.errorMessage = error.message + " " + this.userData.phone
      });
  }

  async enterThePhoneCodeReceived() {
    return new Promise(resolve => {
      this.shared.translateArray(["Enter Sms Code You Received on", "Code", "Cancel", "ok"]).then(async (res: any) => {
        const alert = await this.alertController.create({
          header: res["Enter Sms Code You Received on"] + " " + this.userData.phone,
          inputs: [
            {
              name: 'code',
              type: 'text',
              placeholder: res["Code"],
            }
          ],
          buttons: [
            {
              text: res["Cancel"],
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                resolve(null);
                console.log('Confirm Cancel');
              }
            }, {
              text: res["ok"],
              handler: (data) => {
                console.log(data);
                if (data.code == "")
                  this.enterThePhoneCodeReceived().then(data => {
                    resolve(data);
                  })
                else
                  resolve(data.code);
              }
            }
          ]
        });

        await alert.present();
      });

    });

  }
  loginAfterCodeVerify() {
    this.shared.login(this.userData);
  }



  ngOnInit() {
  }
  ionViewDidEnter() {

    // this.shared.userIsLoggedIn().then((data: any) => {
    //   if (data) {
    //     this.navCtrl.navigateRoot("tabs");
    //   }
    // })
    this.shared.hideSplashScreen();
  }
}
