import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { ConfigService } from 'src/providers/config/config.service';
import { AppEventsService } from 'src/providers/app-events/app-events.service';
import { Router } from '@angular/router';
import { LoadingService } from 'src/providers/loading/loading.service';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  orders = [];
  constructor(
    public nav: NavController,
    public config: ConfigService,
    public appEventsService: AppEventsService,
    public router: Router,
    public loading: LoadingService,
    public modalCtrl: ModalController,
    public shared: SharedDataService
  ) { }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.shared.hideSplashScreen();
  }
  ionViewWillEnter() {
    this.getOrders(null);
  }

  getOrders(value: any) {
    if (this.shared.statusOfBoy == false) return 0;

    this.loading.show();
    this.config.getHttp('orders?password=' + this.shared.customerData.password + '&language_id=1').then((data: any) => {
      this.loading.hide();
      if (data.success == 1) {
        this.orders = data.data;
      }
      if (data.success == 0) {
        this.shared.toast(data.message)
      }
      this.stopRefresh(value);
    });
  }

  stopRefresh(value) {
    if (value != null)
      value.target.complete();
  }

  async showOrderDetail(order) {
    this.shared.orderDetailPageData = order;
    this.nav.navigateForward("tabs/tabs/history/order-detail");
  }

}
