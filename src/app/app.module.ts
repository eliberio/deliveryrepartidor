import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { Network } from '@ionic-native/network/ngx';
import { HTTP } from '@ionic-native/http/ngx';
// Providers Import
import { ConfigService } from '../providers/config/config.service';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
// For Translation
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PipesModule } from 'src/pipes/pipes.module';
import { LoginPageModule } from './modals/login/login.module';
import { BlankModalPageModule } from './modals/blank-modal/blank-modal.module';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Md5 } from 'ts-md5/dist/md5';
import { FCM } from '@ionic-native/fcm/ngx';
import { Device } from '@ionic-native/device/ngx';
import { GoogleMaps } from '@ionic-native/google-maps';
import { RouteReuseStrategy } from '@angular/router';
import { ModalPageModule } from './modal/modal.module';
import { MyAccountPageModule } from './my-account/my-account.module';
import { OrderDetailPageModule } from './order-detail/order-detail.module';
import { MapPageModule } from './map/map.module';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation/ngx';
import { AuthGuardService } from 'src/providers/auth-guard/auth-guard.service';
import { GetIpAddressService } from 'src/providers/get-ip-Address/get-ip-address.service';
import { GetDeviceIdService } from 'src/providers/get-device-id/get-device-id.service';
import { BackButtonExitAppService } from 'src/providers/back-button-exit/back-button-exit-app.service';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      mode: 'md'
    }),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    PipesModule,
    FormsModule,
    BlankModalPageModule,
    ModalPageModule,
    MyAccountPageModule,
    OrderDetailPageModule,
    MapPageModule
  ],
  providers: [
    StatusBar,
    ConfigService,
    BackgroundGeolocation,
    SharedDataService,
    SplashScreen,
    AppVersion,
    OneSignal,
    Geolocation,
    NativeGeocoder,
    Network,
    HTTP,
    PhotoViewer,
    Md5,
    FCM,
    Device,
    GoogleMaps,
    CallNumber,
    LaunchNavigator,
    NetworkInterface,
    AuthGuardService,
    GetIpAddressService,
    GetDeviceIdService,
    UniqueDeviceID,
    BackButtonExitAppService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
