import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuardService as AuthGuard } from "../../providers/auth-guard/auth-guard.service";

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            canActivate: [AuthGuard], loadChildren: () => import('../home-pages/home/home.module').then(m => m.HomePageModule)
          },
          {
            path: 'map',
            canActivate: [AuthGuard], loadChildren: () => import('../map/map.module').then(m => m.MapPageModule)
          },
          {
            path: 'order-detail',
            canActivate: [AuthGuard], loadChildren: () => import('../order-detail/order-detail.module').then(m => m.OrderDetailPageModule)
          }
        ]
      },
      {
        path: 'history',
        children: [
          {
            path: '',
            canActivate: [AuthGuard], loadChildren: () => import('../history/history.module').then(m => m.HistoryPageModule)
          },
          {
            path: 'order-detail',
            canActivate: [AuthGuard],
            loadChildren: () => import('../order-detail/order-detail.module').then(m => m.OrderDetailPageModule)
          }
        ]
      },
      {
        path: 'settings',
        children: [
          {
            path: '',
            canActivate: [AuthGuard], loadChildren: () => import('../settings/settings.module').then(m => m.SettingsPageModule)
          },

          {
            path: 'my-account', canActivate: [AuthGuard], loadChildren: () => import('../my-account/my-account.module').then(m => m.MyAccountPageModule)
          },
          // {
          //   path: 'about-us', loadChildren: () => import('../about-us/about-us.module').then(m => m.AboutUsPageModule)
          // },
        ]
      },

      {
        path: '',
        redirectTo: 'tabs/settings',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/settings',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
