import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';
import { NavController, IonContent } from '@ionic/angular';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { ConfigService } from 'src/providers/config/config.service';
import { NavigationExtras, Router } from '@angular/router';
import { AppEventsService } from 'src/providers/app-events/app-events.service';
import { LoadingService } from 'src/providers/loading/loading.service';
import { OrderDetailPage } from 'src/app/order-detail/order-detail.page';




@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {


  @ViewChild(IonContent, { static: false }) content: IonContent;

  scrollTopButton = false;//for scroll down fab 

  orders = [];

  constructor(
    public nav: NavController,
    public config: ConfigService,
    public appEventsService: AppEventsService,
    public router: Router,
    public loading: LoadingService,
    public modalCtrl: ModalController,
    public shared: SharedDataService) {
  }
  setMethod(d) {
    console.log(d);
  }
  ngOnInit() {
  }
  ionViewDidEnter() {
    this.shared.hideSplashScreen();
  }
  ionViewWillEnter() {
    this.getOrders(null);
  }

  getOrders(value: any) {
    if (this.shared.statusOfBoy == false) return 0;

    this.loading.show();
    this.config.getHttp('orders?password=' + this.shared.customerData.password + '&language_id=1').then((data: any) => {
      this.loading.hide();
      if (data.success == 1) {
        this.orders = data.data;
      }
      if (data.success == 0) {
        this.shared.toast(data.message)
      }

      this.stopRefresh(value);

    });
  }

  stopRefresh(value) {
    if (value != null)
      value.target.complete();
  }


  async showOrderDetail(order) {
    this.shared.orderDetailPageData = order;
    this.nav.navigateForward("tabs/tabs/home/order-detail");
    // let modal = await this.modalCtrl.create({
    //   component: OrderDetailPage,
    //   componentProps: { data: order }
    // });
    // return await modal.present();
  }

  // For FAB Scroll
  onScroll(e) {
    if (e.detail.scrollTop >= 500) {
      this.scrollTopButton = true;
    }
    if (e.detail.scrollTop < 500) {
      this.scrollTopButton = false;
    }
  }
  // For Scroll To Top Content
  scrollToTop() {
    this.content.scrollToTop(700);
    this.scrollTopButton = false;
  }

}
