import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, AlertController, Platform } from '@ionic/angular';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { LoadingService } from 'src/providers/loading/loading.service';
import { ConfigService } from 'src/providers/config/config.service';
import { UserAddressService } from 'src/providers/user-address/user-address.service';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.page.html',
  styleUrls: ['./order-detail.page.scss'],
})
export class OrderDetailPage implements OnInit {

  order: any = {};
  comments: any = "";
  statusToPass: any;
  externalMapUrl = ""
  constructor(
    public modalCtrl: ModalController,
    public shared: SharedDataService,
    public navCtrl: NavController,
    public nav: NavController,
    public loading: LoadingService,
    public config: ConfigService,
    private callNumber: CallNumber,
    public userAddress: UserAddressService,
    public alertController: AlertController,
    public platform: Platform,
    private launchNavigator: LaunchNavigator
  ) {
    this.order = this.shared.orderDetailPageData;

    if (this.config.appSettings.maptype == "external") {
      this.userAddress.getCordinates().then((res: any) => {
        // //res.lat, res.long
        let myLoc = (res.lat).toString() + "," + (res.long).toString();
        // ios
        if (this.platform.is('ios')) {
          this.externalMapUrl = 'maps://?' + 'saddr=' + myLoc + '&daddr=' + this.order.delivery_latitude + ',' + this.order.delivery_longitude;
        };
        //window.open('geo://' + myLoc + '?q=' + "31.5204" + ',' + "74.3587", '_system');
        // android
        if (this.platform.is('android')) {
          this.externalMapUrl = 'geo://' + myLoc + '?q=' + this.order.delivery_latitude + ',' + this.order.delivery_longitude;
        };
        //console.log(res)
      });

    }
  }

  navigate() {
    console.log("naviage click");


    console.log("naviage click google map");
    this.shared.mapPageData = { lat: this.order.delivery_latitude, long: this.order.delivery_longitude };
    this.nav.navigateForward("tabs/tabs/home/map");

  }
  makeCall(phone) {
    this.callNumber.callNumber(phone, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }
  cancelOrder() {
    this.statusToPass = 3
    this.verifyBeforeChangeStatus();
  }
  finishOrder() {
    this.statusToPass = 6
    this.verifyBeforeChangeStatus();
  }
  finishOrderDelivery(){
    let dat:any =  {};
    dat.orders_id = this.order.orders_id;
    dat.id = this.shared.customerData.id;

    this.loading.show();
    this.config.postHttp('finishorderdelivery',dat).then(
        (data:any)=>{
          this.loading.hide();
          this.shared.toast(data.message)
          if (data.success == 1) {
            this.navCtrl.back()
            this.shared.toast(data.message);
          }
          if (data.success == 0) {
            this.shared.toast(data.message);
          }
        }
    ).catch(e=>this.shared.toast(e.message))
  }
  changeOrderStatus() {
    let status = this.statusToPass
    let comments = this.comments;
    let pass = this.shared.customerData.password;
    let orderId = this.order.orders_id;
    this.loading.show();
    this.config.getHttp("changeorderstatus?password=" + pass + "&orders_id=" + orderId + "&orders_status_id=" + status + "&comments=" + comments).then((data: any) => {
      this.loading.hide();
      if (data.success == 1) {
        this.navCtrl.back()
        this.shared.toast(data.message);
      }
      if (data.success == 0) {
        this.shared.toast(data.message);
      }
    });
  }
  verifyBeforeChangeStatus() {
    this.shared.translateArray(["Change Status", "User Pin", "Comments", "Code", "Cancel", "ok"]).then(async (res: any) => {
      const alert = await this.alertController.create({
        header: res["Change Status"],
        inputs: [
          {
            name: 'pass',
            type: 'text',
            placeholder: res["User Pin"],
          },
          {
            name: 'comments',
            type: 'text',
            placeholder: res["Comments"],
          }
        ],
        buttons: [
          {
            text: res["Cancel"],
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {

              console.log('Confirm Cancel');
            }
          }, {
            text: res["ok"],
            handler: (data) => {
              this.comments = data.comments;
              if (this.shared.customerData.password != data.pass)
                this.shared.toastMiddle("Wrong Pincode!")
              else {
                this.changeOrderStatus();
              }
              console.log(data);

            }
          }
        ]
      });

      await alert.present();
    });
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }
  ngOnInit() {
  }

}
