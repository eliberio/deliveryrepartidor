import { Component, OnInit, ApplicationRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/providers/config/config.service';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { NavController, ModalController } from '@ionic/angular';
import { LoadingService } from 'src/providers/loading/loading.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.page.html',
  styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage implements OnInit {

  constructor(
    public config: ConfigService,
    public shared: SharedDataService,
    private photoViewer: PhotoViewer,
    public modalCtrl: ModalController,
    public loading: LoadingService) {
  }
  //close modal
  dismiss() {
    this.modalCtrl.dismiss();
  }
  getNameFirstLetter() {
    return this.shared.getNameFirstLetter();
  }

  zoomImage(img) {
    this.photoViewer.show(img);
  }
  //============================================================================================

  ionViewWillEnter() {

  }

  ngOnInit() {
  }

}
